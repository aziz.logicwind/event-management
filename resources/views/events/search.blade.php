@extends('events.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>@sortablelink('id')</th>
                        <th>@sortablelink('name')</th>
                        <th>Description</th>
                        <th>Members</th>
                        <th>@sortablelink('event_date', 'Event Date')</th>
                        <th>@sortablelink('event_time', 'Event Time')</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($events) > 0)
                        @foreach ($events as $event)
                            <tr>
                                @php
                                    $user = App\Models\EventUser::where('event_id', $event->id)
                                        ->with('user')
                                        ->get();
                                @endphp
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $event->name }} <span class="badge badge-info">{{ count($user) }}</span> </td>
                                <td>{{ $event->description }}</td>
                                <td>
                                    <ul>
                                        @foreach ($user as $item)
                                            <li>{{ $item->user->name }}
                                                @if ($item->is_accept)
                                                    <span class="badge badge-pill badge-success">Accepted</span>
                                                @else
                                                    <span class="badge badge-pill badge-warning">Pending</span>
                                                @endif
                                                <form action="{{ url('remove-member/' . $item->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="badge badge-danger">Remove</button>
                                                </form>
                                            </li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ $event->event_date }}</td>
                                <td>{{ $event->event_time }}</td>
                                <td>
                                    @if ($event->user_id == Auth::user()->id)
                                        <a class="btn btn-primary btn-sm m-1"
                                            href="{{ url('invite-members/' . $event->id) }}">Invite</a>
                                        <form action="{{ route('events.destroy', $event->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a class="btn btn-warning btn-sm" href="{{url('events/'.$event->id.'/edit')}}">Update</a>
                                            <button type="submit" class="btn btn-danger btn-sm m-1">Delete</button>
                                        </form>
                                    @else
                                        {{-- <a href="{{ route('events.show', $event->id) }}"
                                            class="btn btn-primary btn-sm">Join</a> --}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <span class="badge badge-pill badge-danger m-2">Oops! No Event found</span>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('events.addevent-modal')
@endsection
