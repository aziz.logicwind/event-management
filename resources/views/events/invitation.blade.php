@extends('events.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Event Date</th>
                        <th>Event Time</th>
                        <th>@sortablelink('is_accept', 'Action')</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($invitation) > 0)
                        @foreach ($invitation as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->event->name }}</td>
                                <td>{{ $item->event->description }}</td>
                                <td>{{ $item->event->event_date }}</td>
                                <td>{{ $item->event->event_time }}</td>
                                <td>
                                    @if ($item->is_accept)
                                        <span class="badge badge-pill badge-success p-2">Accepted</span>
                                    @else
                                        <a class="btn btn-primary btn-sm m-1"
                                            href="{{ url('accept-invitation/' . $item->id) }}">Accept</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <span class="badge badge-pill badge-danger m-2">Oops! No invitaation</span>
                    @endif
                </tbody>
            </table>
            {{ $invitation->links() }}
        </div>
    </div>
    @include('events.addevent-modal')
@endsection
