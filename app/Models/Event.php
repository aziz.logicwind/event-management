<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Event extends Model
{
    use HasFactory, Sortable;
    public $fillable = [
        'name',
        'description',
        'event_date',
        'event_time',
        'user_id',
    ];
    public $sortable = [
        'id',
        'name',
        'event_date',
        'event_time',
    ];

    //time format
    public function getEventTimeAttribute($value)
    {
        return date('h:i A', strtotime($value));
    }

    //event created user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //event invite users
    public function invite()
    {
        return $this->hasMany(EventUser::class);
    }

//     public function eventusers()
//     {
//         return $this->hasMany(EventUser::class, 'event_id', 'id');
//     }
}
